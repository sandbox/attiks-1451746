<?php
/**
 * @file
 * Views handler for the dummy count field.
 */
class views_handler_field_dummycount extends views_handler_field_numeric {
  function query() {
    // skip this field in the real query
  }

  function render($values) {
    static $numrows = -1;
    if ($numrows < 0) {
      $numrows = 0;
     $dummycount = new view;
      foreach ($this->view as $key => $val) {
        if (is_object($val) && $key != 'query') {
            $dummycount->{$key} = clone $val;
        }
        elseif (is_array($val)) {
            $dummycount->{$key} = $val;
        }
        else {
            $dummycount->{$key} = $val;
        }
      }
      $dummycount->query = new views_plugin_query_default;
      foreach ($this->view->query as $key => $val) {
        if (is_object($val)) {
            $dummycount->query->{$key} = clone $val;
        }
        elseif (is_array($val)) {
            $dummycount->query->{$key} = $val;
        }
        else {
            $dummycount->query->{$key} = $val;
        }
      }
      $dummycount->query->orderby = array();
      $dummycount->query->fields = array();
      $dummycount->query->fields = $this->view->query->fields;

      // add count field to order by
      $fieldalias = '';
      foreach ($dummycount->query->fields as $field) {
        if (isset($field['function']) && $field['function'] == 'count') {
          $fieldalias = $field['alias'];
          $dummycount->query->orderby[] = array(
              'field' => $fieldalias,
              'direction' => 'DESC',
          );
          $dummycount->limit = 1;
          continue;
        }
      }

      if (!empty($fieldalias)) {
        $dummycount->query->view = $dummycount;
        $dummycount->query->query(FALSE);
        $dummycount->query->build($dummycount);
        $dummycount->query->execute($dummycount);
        if (isset($dummycount->result) && isset($dummycount->result[0])) {
          $numrows = $dummycount->result[0]->{$fieldalias};
        }
      }
    }
    return $numrows;
  }
}