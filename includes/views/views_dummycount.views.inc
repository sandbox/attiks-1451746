<?php
/**
 * @file
 * Views hooks implementations.
 */

/**
 * Implements hook_views_data().
 */
function views_dummycount_views_data() {
  return array(
    'node' => array(
      'dummycount' => array(
        'title' => t('Dummy count'),
        'help' => t('The count of all rows.'),
        'field' => array(
          'handler' => 'views_handler_field_dummycount',
          'click sortable' => FALSE,
        ),
      ),
    ),
  );
}
